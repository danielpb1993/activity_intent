package com.example.layouts2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.ImageView;
import android.widget.TextView;

public class Activity_card_view extends AppCompatActivity {

    private TextView textTitle;
    private TextView productTitle;
    private TextView productDesc;
    private ImageView productImage;
    private ImageView imageTop;
    private ImageView bigImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view);

        textTitle = (TextView) findViewById(R.id.textTitle);
        productTitle = (TextView) findViewById(R.id.productTitle);
        productDesc = (TextView) findViewById(R.id.longDescription);
        imageTop = (ImageView) findViewById(R.id.imageTop);
        productImage = (ImageView) findViewById(R.id.productImage);
        bigImage = (ImageView) findViewById(R.id.bigImage);

        Intent i = getIntent();
        String sProductTitle = i.getStringExtra(MainActivity.PRODUCT_TITLE);
        String sProductDesc = i.getStringExtra(MainActivity.PRODUCT_DESC);
        int iProductImage = i.getIntExtra(MainActivity.PRODUCT_IMAGE, 0);

        textTitle.setText(sProductTitle);
        productTitle.setText(sProductTitle);
        productDesc.setText(sProductDesc);

        imageTop.setImageResource(iProductImage);
        productImage.setImageResource(iProductImage);
        bigImage.setImageResource(iProductImage);
    }
}