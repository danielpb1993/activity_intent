package com.example.layouts2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public static String PRODUCT_TITLE = "com.example.layouts2.PRODUCT_TITLE";
    public static String PRODUCT_IMAGE = "com.example.layouts2.PRODUCT_IMAGE";
    public static String PRODUCT_DESC = "com.example.layouts2.PRODUCT_DESC";

    private CardView cardView11;
    private CardView cardView12;
    private CardView cardView21;
    private CardView cardView22;
    private CardView cardView31;
    private CardView cardView32;
    private CardView cardView41;
    private CardView cardView42;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cardView11 = (CardView) findViewById(R.id.cardView11);
        cardView12 = (CardView) findViewById(R.id.cardView12);
        cardView21 = (CardView) findViewById(R.id.cardView21);
        cardView22 = (CardView) findViewById(R.id.cardView22);
        cardView31 = (CardView) findViewById(R.id.cardView31);
        cardView32 = (CardView) findViewById(R.id.cardView32);
        cardView41 = (CardView) findViewById(R.id.cardView41);
        cardView42 = (CardView) findViewById(R.id.cardView42);

        cardView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle = "Pinguin";
                String productDesc = "Penguins (order Sphenisciformes /sfɪˈnɪsɪfɔːrmiːz/, family Spheniscidae /sfɪˈnɪsɪdiː/) are a group of aquatic flightless birds. They live almost exclusively in the southern hemisphere: only one species, the Galápagos penguin, is found north of the Equator. Highly adapted for life in the water, penguins have countershaded dark and white plumage and flippers for swimming. Most penguins feed on krill, fish, squid and other forms of sea life which they catch while swimming underwater. They spend roughly half of their lives on land and the other half in the sea. ";
                int productImage = R.drawable.animals1;

                Intent intent = new Intent(MainActivity.this, Activity_card_view.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);
                intent.putExtra(PRODUCT_DESC, productDesc);
                startActivity(intent);
            }
        });

        cardView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle = "Panter";
                String productDesc = "A black panther is the melanistic colour variant of the leopard (Panthera pardus) and the jaguar (Panthera onca). Black panthers of both species have excess black pigments, but their typical rosettes are also present. They have been documented mostly in tropical forests, with black leopards in Kenya, India, Sri Lanka, Nepal, Thailand, Peninsular Malaysia and Java, and black jaguars in Mexico, Panama, Costa Rica and Paraguay. Melanism is caused by a recessive allele in the leopard, and by a dominant allele in the jaguar. ";
                int productImage = R.drawable.animals3;


                Intent intent = new Intent(MainActivity.this, Activity_card_view.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);
                intent.putExtra(PRODUCT_DESC, productDesc);
                startActivity(intent);
            }
        });

        cardView21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle = "Rhino";
                String productDesc = "A rhinoceros (/raɪˈnɒsərəs/, from Greek rhinokerōs 'nose-horned', from rhis 'nose', and keras 'horn'), commonly abbreviated to rhino, is a member of any of the five extant species (or numerous extinct species) of odd-toed ungulates in the family Rhinocerotidae. (It can also refer to a member of any of the extinct species of the superfamily Rhinocerotoidea.) Two of the extant species are native to Africa, and three to South and Southeast Asia. ";
                int productImage = R.drawable.animals2;

                Intent intent = new Intent(MainActivity.this, Activity_card_view.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);
                intent.putExtra(PRODUCT_DESC, productDesc);
                startActivity(intent);
            }
        });

        cardView22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle = "Crocodile";
                String productDesc = "Crocodiles (family Crocodylidae) or true crocodiles are large semiaquatic reptiles that live throughout the tropics in Africa, Asia, the Americas and Australia. The term crocodile is sometimes used even more loosely to include all extant members of the order Crocodilia, which includes the alligators and caimans (family Alligatoridae), the gharial and false gharial (family Gavialidae), and all other living and fossil Crocodylomorpha. ";
                int productImage = R.drawable.animals4;

                Intent intent = new Intent(MainActivity.this, Activity_card_view.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);
                intent.putExtra(PRODUCT_DESC, productDesc);
                startActivity(intent);
            }
        });

        cardView31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle = "Wolf";
                String productDesc = "The wolf (Canis lupus[a]), also known as the gray wolf or grey wolf, is a large canine native to Eurasia and North America. More than thirty subspecies of Canis lupus have been recognized, and gray wolves, as colloquially understood, comprise non-domestic/feral subspecies. The wolf is the largest extant member of the family Canidae, males averaging 40 kg (88 lb) and females 37 kg (82 lb). Wolves measure 105–160 cm (41–63 in) in length and 80–85 cm (31–33 in) at shoulder height. ";
                int productImage = R.drawable.animals5;

                Intent intent = new Intent(MainActivity.this, Activity_card_view.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);
                intent.putExtra(PRODUCT_DESC, productDesc);
                startActivity(intent);
            }
        });

        cardView32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle = "Hippo";
                String productDesc = "The hippopotamus (/ˌhɪpəˈpɒtəməs/ HIP-ə-POT-ə-məs;[3] Hippopotamus amphibius), also called the hippo, common hippopotamus or river hippopotamus, is a large, mostly herbivorous, semiaquatic mammal and ungulate native to sub-Saharan Africa. It is one of only two extant species in the family Hippopotamidae, the other being the pygmy hippopotamus (Choeropsis liberiensis or Hexaprotodon liberiensis). The name comes from the ancient Greek for \"river horse\" (ἱπποπόταμος). ";
                int productImage = R.drawable.animals6;

                Intent intent = new Intent(MainActivity.this, Activity_card_view.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);
                intent.putExtra(PRODUCT_DESC, productDesc);
                startActivity(intent);
            }
        });

        cardView41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle = "Monkey";
                String productDesc = "Monkey is a common name that may refer to most mammals of the infraorder Simiiformes, also known as the simians. Traditionally, all animals in the group now known as simians are counted as monkeys except the apes, a grouping known as paraphyletic; however in the broader sense based on cladistics, apes (Hominoidea) are also included, making the terms monkeys and simians synonyms in regard of their scope.";
                int productImage = R.drawable.animals7;

                Intent intent = new Intent(MainActivity.this, Activity_card_view.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);
                intent.putExtra(PRODUCT_DESC, productDesc);
                startActivity(intent);
            }
        });

        cardView42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle = "Bull";
                String productDesc = "A bull is an intact (i.e., not castrated) adult male of the species Bos taurus. More muscular and aggressive than the females of the same species, the cows, bulls have long been an important symbol in many cultures, and play a significant role in beef ranching, dairy farming, and a variety of other cultural activities, including bullfighting and bull riding. ";
                int productImage = R.drawable.animals8;

                Intent intent = new Intent(MainActivity.this, Activity_card_view.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);
                intent.putExtra(PRODUCT_DESC, productDesc);
                startActivity(intent);
            }
        });
    }
}